import java.util.Calendar;

public class Main {

    public static void main(String[] args) {

        Calendar inicio = Calendar.getInstance();
        Calendar hoje = Calendar.getInstance();

        inicio.set(Calendar.DAY_OF_MONTH, 17);
        inicio.set(Calendar.MONTH, Calendar.FEBRUARY);
        inicio.set(Calendar.YEAR, 2021);

        int diaDeInicio = inicio.get(Calendar.DAY_OF_MONTH);
        int mesDeInicio = inicio.get(Calendar.MONTH)+1; // +1 pois começa com indice zero
        int anoDeInicio = inicio.get(Calendar.YEAR);

        int diaAtual = hoje.get(Calendar.DAY_OF_MONTH);
        int mesAtual = hoje.get(Calendar.MONTH)+1; // +1 pois começa com indice zero
        int anoAtual = hoje.get(Calendar.YEAR);
        int totalDiasDoMes = hoje.getActualMaximum(Calendar.DAY_OF_MONTH);

        Salario meuSalario = new Salario(2500.00,100.00);

        RescisaoSemJustaCausa rsjc = new RescisaoSemJustaCausa(diaDeInicio, mesDeInicio, anoDeInicio, diaAtual, mesAtual, anoAtual, totalDiasDoMes);

        System.out.println("Data de Entrada: " + diaDeInicio + "/" + mesDeInicio + "/" + anoDeInicio);
        System.out.println("Data de Saída  : " + diaAtual + "/" + mesAtual + "/" + anoAtual);
        System.out.println("Total de dias do mês atual: " + totalDiasDoMes);

        System.out.println("---------------");

        System.out.println("Salário    -> R$: " + meuSalario.getSalario());
        System.out.println("Benefícios -> R$: " + meuSalario.getBeneficio());

        System.out.println("---------------");

        Double totalSalario = rsjc.calulaSaldoDeSalario(meuSalario);
        System.out.println("Saldo de Salário          -> + R$: " + totalSalario);

        Double totalBeneficios = rsjc.calulaSaldoDeBeneficios(meuSalario);
        System.out.println("Saldo de Benefícios       -> + R$: " + totalBeneficios);

        Double avisoPrevio = rsjc.calculaAvisoPrevio(meuSalario);
        System.out.println("Aviso Prévio              -> + R$: " + avisoPrevio);

        Double decimoTerceiroProporcional = rsjc.calculaDecimoTerceiroProporcional(meuSalario);
        System.out.println("13º proporcional:         -> + R$: " + decimoTerceiroProporcional);

        Double decimoTerceiroIndenizado = rsjc.calculaDecimoTerceiroIndenizado(meuSalario);
        System.out.println("13º indenizado            -> + R$: " + decimoTerceiroIndenizado);

        Double feriasProporcional = rsjc.calculaFeriasProporcionais(meuSalario);
        System.out.println("Férias proporcionais:     -> + R$: " + feriasProporcional);

        Double umTercoFeriasProporcionais = rsjc.calculaUmTercoFeriasProporcionais(meuSalario);
        System.out.println("1/3 Férias proporcionais: -> + R$: " + umTercoFeriasProporcionais);

        Double feriasIndenizado = rsjc.calculaFeriasIndenizado(meuSalario);
        System.out.println("Férias indenizadas:       -> + R$: " + feriasIndenizado);

        Double umTercoferiasIndenizado = rsjc.calculaUmTercoFeriasIndenizado(meuSalario);
        System.out.println("1/3 Férias indenizadas:   -> + R$: " + umTercoferiasIndenizado);

        Double valorFGTS = rsjc.calculaFGTS(meuSalario);
        System.out.println("valor FGTS:               -> + R$: " + valorFGTS);

        Double multaSobreFGTS = rsjc.calculaMultaSobreFGTS(meuSalario);
        System.out.println("Multa sobre FGTS:         -> + R$: " + multaSobreFGTS);

        System.out.println("---------------");

        Double totalSemFGTS = rsjc.somaRecebimentosSemFGTS(meuSalario);
        System.out.println("TOTAL DE RECEBIMENTOS SEM FGTS    =    R$: " + totalSemFGTS);

        Double totalComFGTS = rsjc.somaRecebimentosComFGTS(meuSalario);
        System.out.println("TOTAL DE RECEBIMENTOS COM FGTS    =    R$: " + totalComFGTS);

        System.out.println("---------------");

        System.out.println(" Descontos a implementar ... ");

    }
}
