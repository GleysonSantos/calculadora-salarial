public class RescisaoSemJustaCausa {

    private final int diaDeInicio;
    private final int mesDeInicio;
    private final int anoDeInicio;
    private final int diaAtual;
    private final int mesAtual;
    private final int anoAtual;
    private final int QtdDiasDoMesAtual;

    public RescisaoSemJustaCausa(int diaDeInicio, int mesDeInicio, int anoDeInicio, int diaAtual, int mesAtual, int anoAtual, int QtdDiasDoMesAtual) {
        this.diaDeInicio = diaDeInicio;
        this.mesDeInicio = mesDeInicio;
        this.anoDeInicio = anoDeInicio;
        this.diaAtual = diaAtual;
        this.mesAtual = mesAtual;
        this.anoAtual = anoAtual;
        this.QtdDiasDoMesAtual = QtdDiasDoMesAtual;
    }

    // SALDO DE SALÁRIO
    public Double calulaSaldoDeSalario(Salario salario){
        return (salario.getSalario() / QtdDiasDoMesAtual) * diaAtual;
    }

    // SALDO DE BENEFÍCIOS
    public Double calulaSaldoDeBeneficios(Salario salario){
        return (salario.getBeneficio() / QtdDiasDoMesAtual) * diaAtual;
    }

    // SALDO DE AVISO PRÉVIO
    public Double calculaAvisoPrevio(Salario salario){
        return salario.getSalario();
    }

    // 13° PROPORCIONAL
    public Double calculaDecimoTerceiroProporcional(Salario salario){
        if (anoDeInicio == anoAtual){
            return (salario.getSalario() / 12) * (mesAtual - mesDeInicio);
        } else {
            return (salario.getSalario() / 12) * mesAtual;
        }
    }

    // 13° INDENIZADO
    public Double calculaDecimoTerceiroIndenizado(Salario salario){
        return (salario.getSalario() / 12) * 1;
    }

    // FÉRIAS PROPORCIONAL
    public Double calculaFeriasProporcionais(Salario salario){
        if (anoDeInicio == anoAtual){
            if(diaAtual >= 14){
                return (salario.getSalario() / 12) * (mesAtual - mesDeInicio);
            } else {
                return (salario.getSalario() / 12) * ((mesAtual - mesDeInicio) - 1);
            }
        } else {
            if(diaAtual >= 14){
                return (salario.getSalario() / 12) * mesAtual;
            } else {
                return (salario.getSalario() / 12) * (mesAtual - 1);
            }
        }
    }

    // 1/3 FÉRIAS PROPORCIONAL
    public Double calculaUmTercoFeriasProporcionais(Salario salario){
        return calculaFeriasProporcionais(salario) * 1/3;
    }

    // FÉRIAS INDENIZADO
    public Double calculaFeriasIndenizado(Salario salario){
        return (salario.getSalario() / 12) * 1;
    }

    // 1/3 FÉRIAS INDENIZADO
    public Double calculaUmTercoFeriasIndenizado(Salario salario){
        return calculaFeriasIndenizado(salario) * 1/3;
    }

    // FGTS
    public Double calculaFGTS(Salario salario){
        return 1470.0;
        // calcular utilisando a diferença de dias entre a data de entrada e a data saida
        // dividir este resultado pela quantidade de dias do mes atual
        // multiplicar o resultado pelo salario * 0.8

    }

    // MULTA SOBRE FGTS
    public Double calculaMultaSobreFGTS(Salario salario){
        return calculaFGTS(salario) * 0.4;
    }

    // ------------------------------------------------------------------------------------------ //

    public Double somaRecebimentosSemFGTS(Salario salario){
        return calulaSaldoDeSalario(salario) +
               calulaSaldoDeBeneficios(salario) +
               calculaAvisoPrevio(salario) +
               calculaDecimoTerceiroProporcional(salario) +
               calculaDecimoTerceiroIndenizado(salario) +
               calculaFeriasProporcionais(salario) +
               calculaUmTercoFeriasProporcionais(salario) +
               calculaFeriasIndenizado(salario) +
               calculaUmTercoFeriasIndenizado(salario);
    }

    public Double somaRecebimentosComFGTS(Salario salario){
        return somaRecebimentosSemFGTS(salario) + calculaFGTS(salario) + calculaMultaSobreFGTS(salario);
    }

    // ------------------------------------------------------------------------------------------ //

}

//    INSS - SALÁRIO
//    IRRF - SALÁRIO
//    INSS - AVISO
//    IRRF - AVISO
//    INSS - 13º PROPORCIONAL
//    IRRF - 13º PROPORCIONAL
//    INSS - 13º INDENIZADO
//    IRRF - 13º INDENIZADO
//    INSS - FÉRIAS PROPORCIONAL
//    IRRF - FÉRIAS PROPORCIONAL
//    INSS - FÉRIAS INDENIZADO
//    IRRF - FÉRIAS INDENIZADO
