public class Salario {

    private Double salario;
    private Double beneficio;

    public Salario(Double salario, Double beneficio) {
        this.salario = salario;
        this.beneficio = beneficio;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Double getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(Double beneficio) {
        this.beneficio = beneficio;
    }

    @Override
    public String toString() {
        return "Salario = R$ " + this.salario + " / " + "Beneficios = R$ " + this.beneficio;
    }
}
